# README #

This is my first assignment for PIC 10C. I have created a game called Siete y Medio that is similar to blackjack. My main goal of this project has been getting comfortable using git.


### Operating the Game ###

* The game will start by asking the user for a bet. If you attempt to bet more money than you have, the game will prompt you to enter a different bet.
* You will be "dealt" a card and told it's numeric value. You can then continue asking for more cards until you are satisfied.
* At this point, the game will draw cards for the dealer and display them. The player that comes closer to a 7.5 total wins.
* Your money will be adjusted and you will be prompted to bet again.
* This will continue until either you lose all of your money or the dealer loses $900

### Code Breakdown ###

* One header file: cards_2.h_
* Two source files: cards_2.cpp and siete_y_medio_2.cpp

### Example of Gameplay ###

This example displays one round of the game:

You have $100. Enter bet: 50

New card:
        Sota de copas (Jack of cups).

Your cards:
        Sota de copas (Jack of cups).
        
Your total is 0.5. Do you want another card (y/n)? y

New card:
        As de espadas (Ace of swords).

Your cards:
        Sota de copas (Jack of cups).
        As de espadas (Ace of swords).
        
Your total is 1.5. Do you want another card (y/n)? y

New card:
        Cinco de copas (Five of cups).

Your cards:
        Sota de copas (Jack of cups).
        As de espadas (Ace of swords).
        Cinco de copas (Five of cups).
        
Your total is 6.5. Do you want another card (y/n)? y

New card:
        Caballo de espadas (Knight of swords).

Your cards:
        Sota de copas (Jack of cups).
        As de espadas (Ace of swords).
        Cinco de copas (Five of cups).
        Caballo de espadas (Knight of swords).
        
Your total is 7. Do you want another card (y/n)? n

Dealer's cards:

New card:
        Cinco de espadas (Five of swords).

Dealer's cards:
        Cinco de espadas (Five of swords).
        
The dealer's total is 5.

New card:
        Seis de copas (Six of cups).

Dealer's cards:
        Cinco de espadas (Five of swords).
        Seis de copas (Six of cups).
        
The dealer's total is 11.

You win 50!!!

You have $150. Enter bet:
